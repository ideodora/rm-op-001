#!/usr/bin/env node
/**
 *	postinstall
 *
 *	create a symlink in 'node_modules' to allow non-relative local 'require'
 *		require('_/public') // loads {root}/script/public/index.js
 *
 *	set as npm preinstall command
 *		{
 *			"scripts": {
 *				"postinstall": "node postinstall.js"
 *			}
 *		}
 */
var fs = require('fs');
var path = require('path');

var symlink		=	"app";
var libDir		=	path.join(__dirname, 'scripts');
var reqDir		=	path.join(__dirname, 'node_modules');
var symLinkPath	=	path.join(reqDir, symlink);

if ( ! fs.existsSync(symLinkPath) ) {
	fs.symlinkSync(libDir, symLinkPath, 'dir');
}